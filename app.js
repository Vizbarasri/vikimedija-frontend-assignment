const express = require('express');
var bodyParser = require("body-parser");
const app = express();
const path = require('path');
const database = require('./db');
const file = './db.json';
const jsonfile = require('jsonfile')


app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/', (req, res) => res.sendFile(path.join(__dirname + '/index.html')));

app.get('/database', (req, res, next) => {
    const send = JSON.stringify(database);
    res.send(send);
    console.log('database send');

});

var fs = require('fs');

app.post('/database', (req, res) => {
    console.log(req.body);
    const got = req.body;

    fs.readFile(file, 'utf8', function readFileCallback(err, data) {
        if (err) {
            console.log(err);
        } else {
            obj = JSON.parse(data); 
            obj.push(got); 
            json = JSON.stringify(obj); 
            fs.writeFile(file, json, 'utf8', function (err) {
                if (err) {
                    return console.log(err);
                }
                console.log("File saved successfully!");
            }); 
        }
    });


    console.log('posted to database');
});

app.listen(3000);